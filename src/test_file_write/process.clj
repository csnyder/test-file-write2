(ns test-file-write.process
  (:require [clojure.string :as s]
            [test-file-write.data :as d]
            [clojure.data.json :as json]
            [ras-db-library.db.export :as db-export]
            [clojure.java.io :as io]))

;; --------------------------------------------------------
;; util.clj
;;---------------------------------------------------------
(defn check-empty-data  [data]
      (if  (empty? data) [] data))
;; --------------------------------------------------------
;; salisfy.clj
;;---------------------------------------------------------

(defn format-view-data  [labels data-row]
  (map #(get data-row  (keyword  (s/lower-case %)))
       labels))
  
(defn build-salsify  [data] ;; LABELs is normally pulled from a DB
  (let  [labels  d/LABELS]
    (concat  labels
            (into [] (map #(format-view-data labels %) data) ))))
  
  (defn get-salsify  []  
    (clojure.pprint/pprint "get-data")
    (let  [payload  (db-export/get-salsify)]  
      ; come from the sql statment on ras-db-library 
      ;SELECT *  FROM exportvw_Salsifytest; 
      ;View is Complex and takes about 23 secs  
      ;to return with 3050 rows and 36 Columns
    (clojure.pprint/pprint "payload")
       (print (time (spit "testout.json"  (json/write-str payload)) ) ) ;; here is the block
    (clojure.pprint/pprint "write-data-payload")
      (check-empty-data payload)))


;; --------------------------------------------------------
;; file.clj
;;---------------------------------------------------------

(defn get-data  [fn-build fn-data]
  (let  [payload  (fn-data)]
    (fn-build payload)))

(defn write-file  [fpath data]
  (try 
    (clojure.pprint/pprint "write-file")
   ; (if  (not  (nil?  (io/make-parents fpath)))
   ;   (spit fpath (into [] data))
   ;   (throw  (Exception.  ""))) 
    (clojure.pprint/pprint "write-file2")
    (catch Exception e
      (throw  (Exception.  (str  "Problem Writing File : " fpath))))))


